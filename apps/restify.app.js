/*
Restify
http://mcavage.me/node-restify/
*/
var restify = require('restify');

function respond(req, res, next) {
  res.send({
  	'text': 'hello ' + req.params.name
  });

  next();
}

var server = restify.createServer();
server.get('/', function(req, res, next) {
	res.send({
		'data': {
			'title': 'restify: here',
			'id': 7
		}
	});
});
server.get('/hello/:name', respond);

exports.app = server;
exports.server = server;