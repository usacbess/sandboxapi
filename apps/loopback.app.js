/*
Loopback
http://loopback.io/
*/
var loopback = require('loopback');
var Item = {
	title: 'This is a loopback title',
	id: 7
};
var app = module.exports = loopback();
 
// app.model(Item);
app.use('/api', loopback.rest());

exports.app = app;