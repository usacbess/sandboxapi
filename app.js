// Start the various servers
// ref: http://strongloop.com/strongblog/compare-express-restify-hapi-loopback/

var port = 8000;

// Express 4.x - http://expressjs.com/4x/api.html
var expressApp = require('./apps/express.app').app;
expressApp.set('port', ++port);
expressApp.listen(expressApp.get('port'), function() {
    console.log('Express server listening on port ' + expressApp.get('port'));
});

var restifyApp = require('./apps/restify.app').server;
restifyApp.listen(++port, function() {
  console.log('%s listening at %s', restifyApp.name, restifyApp.url);
});

// Hapi
// http://hapijs.com/tutorials
var hapiApp = require('./apps/hapi.app');
hapiApp.createServer(function(hapi) {
	var server = new hapi.Server(++port);
	return server;
});
hapiApp.run();

// loopback
var loopbackApp = require('./apps/loopback.app');
loopbackApp.listen(++port);
